import axios from "axios";
import config from "@/config";

let client = axios.create({
    baseURL: config.baseAPI,
    timeout: 500000,
    maxRedirects: 5
});

const request = async (options) => {
    const onSuccess = function (response) {
        return response;
    };
 
    const onError = function (error) {
        return Promise.reject(error.response || error.message);
    };
 
    try {
        const response = await client(options);
        return onSuccess(response);
    } catch (error) {
        return onError(error);
    }
};
 
 export default request;
 