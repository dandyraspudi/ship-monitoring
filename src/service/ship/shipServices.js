import request from "../../network/request";

const getVessel = () => {
    let token = localStorage.getItem('access_token');
    return request({
        url: 'vessel/myvessel',
        method: "GET",
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'X-Custom-Header': 'foobar',
            'Authorization': 'Bearer ' + token
        }
    });
};

const getLastPositionVessel = () => {
    let token = localStorage.getItem('access_token');
    return request({
        url: 'location/last-position',
        method: "GET",
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'X-Custom-Header': 'foobar',
            'Authorization': 'Bearer ' + token
        }
    });
};

const getHistoryVessel = (id) => {
    let token = localStorage.getItem('access_token');
    return request({
        url: `location/history/${id}`,
        method: "GET",
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'X-Custom-Header': 'foobar',
            'Authorization': 'Bearer ' + token
        }
    });
};

export const shipServices = {
    getVessel,
    getLastPositionVessel,
    getHistoryVessel
};