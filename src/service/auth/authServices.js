import authRequest from "../../network/auth";

const doLogin = (payload) => {
    return authRequest({
        url: 'auth/signin',
        method: "POST",
        data: payload
    });
};

const doSignup = (payload) => {
    return authRequest({
        url: 'auth/signup',
        method: "POST",
        data: payload
    });
};

export const authServices = {
    doLogin,
    doSignup
};