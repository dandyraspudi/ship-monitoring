import Vue from "vue";
import VueRouter from "vue-router";
import DashboardLayout from '../layout/DashboardLayout.vue'
import AuthLayout from '../layout/AuthLayout.vue'
// GeneralViews
import NotFound from '../pages/NotFoundPage.vue'

// Admin pages
import Login from 'src/pages/Login.vue'
import RegisterUser from 'src/pages/RegisterUser.vue'
import Overview from 'src/pages/Overview.vue'
import UserProfile from 'src/pages/UserProfile.vue'
import Ship from 'src/pages/Ship.vue'
import TableList from 'src/pages/TableList.vue'
import Typography from 'src/pages/Typography.vue'
import Icons from 'src/pages/Icons.vue'
import Maps from 'src/pages/Maps.vue'
import Notifications from 'src/pages/Notifications.vue'
import HistoryVessel from 'src/pages/HistoryVessel.vue'

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  linkActiveClass: "nav-item active",
  routes: [
    {
      path: '/auth',
      component: AuthLayout,
      redirect: '/auth/login',
      children: [
        {
          path: 'login',
          component: Login,
          name: 'login'
        },
        {
          path: 'register',
          component: RegisterUser,
          name: 'registerUser'
        },
      ]
    },
    {
      path: '/admin',
      component: DashboardLayout,
      redirect: '/admin/overview',
      children: [
        {
          path: 'overview',
          name: 'Overview',
          component: Overview
        },
        {
          path: 'user',
          name: 'User',
          component: UserProfile
        },
        {
          path: 'ship',
          name: 'Ship',
          component: Ship
        },
        {
          path: 'table-list',
          name: 'Table List',
          component: TableList
        },
        {
          path: 'typography',
          name: 'Typography',
          component: Typography
        },
        {
          path: 'icons',
          name: 'Icons',
          component: Icons
        },
        {
          path: 'maps',
          name: 'Maps',
          component: Maps
        },
        {
          path: 'notifications',
          name: 'Notifications',
          component: Notifications
        },
        {
          path: 'history',
          name: 'History Vessel',
          component: HistoryVessel
        }
      ]
    },
    { path: '*', component: NotFound }
  ],
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

// const auth = {
//   loggedIn() {
//       return localStorage.getItem("access_token")
//   },
//   logout() {
//       localStorage.removeItem('access_token');
//   }
// }

// router.beforeEach((to, from, next) => {
//   let authrequired = false
//   if (to && to.meta && to.meta.auth) authrequired = true
//   if (authrequired) {
//       if (auth.loggedIn()) {
//           if (to.name === "login") {
//               next({
//                 path: '/#/admin/overview'
//               })
//               return false
//           }
//       } else {
//           if (to.name !== "login") {
//               next({
//                 path: '/#/auth/login'
//               })
//               return false
//           }
//       }
//   } else {
//       if (auth.loggedIn() && to.name === "login") {
//           next({
//             path: '/#/admin/overview'
//           })
//           return false
//       } else {
//         next({
//           path: '/#/auth/login'
//         })
//         return false
//       }
//   }
// })

export default router
